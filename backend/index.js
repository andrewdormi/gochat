// const database = require('./config/database');
const path = require('path');

const express = require('express');
const config = require('./config');
const logger = require('./utils/logger');
const app = express();

const server = require('http').createServer(app);
const io = require('socket.io')(server);

const dbconnection = require('./dbconnection/MongoConnection');

const IOController = require('./controller/IOController');

app.use(express.static(path.resolve(__dirname, '..', 'frontend', 'build')));

// Always return the main index.html, so react-router render the route in the client
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'frontend', 'build', 'index.html'));
});

process.on('uncaughtException', function (err) {
    logger.error('uncaughtException', err);
});

process.on('uncaughtRejection', function (err) {
    logger.error('uncaughtRejection', err);
});

server.listen(config.port, () => {
    IOController.init(io);
    logger.info(`App listening on port ${config.port}!`);
});

module.exports = app;
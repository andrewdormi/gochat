const logger = require('../utils/logger');

const UserRoute = require('../router/UserRoute');
const UserController = require('./UserController');

class IOController {
    init(io) {
        this.io = io;
        const routes = this.createRoutes();
        this.io.on('connection', (socket)=> {
            logger.info('socket connected');

            routes.forEach((route)=> {
                route.addSocket(socket);
            });
        });
    }

    createRoutes() {
        let routes = [];
        routes.push(new UserRoute(UserController));
        return routes;
    }
}

module.exports = new IOController();

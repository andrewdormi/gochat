const logger = require('../utils/logger');
const Aurhorization = require('../domain/Authorization');

class UserController {
    async auth(socket, data, cb) {
        const {type} = data;
        const availableTypes = ['vk'];

        try {
            if (!type || !availableTypes.includes(type)) {
                throw new Error({message: 'Wrong authorization type', code: 500});
            }

            if (type === 'vk') {
                const user = await Aurhorization.authWithVk(data, cb);
                return cb && cb(null, user);
            }
        } catch (err) {
            cb && cb(err);
        }
    }

    async disconnect(socket) {
        logger.info('DISCONNECT');
    }
}

module.exports = new UserController();
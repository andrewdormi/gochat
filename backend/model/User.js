const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var UserSchema = new Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    socialNetwork: {
        type: String
    },
    socialId: {
        type: String
    },
    socialToken: {
        type: String
    }
});

UserSchema.methods = {};

module.exports = mongoose.model('User', UserSchema);

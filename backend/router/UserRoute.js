const Router = require('./Router');

class UserRoute extends Router {
    constructor(controller) {
        super(controller);
        this.events = ['user:auth', 'disconnect'];
    }
}

module.exports = UserRoute;

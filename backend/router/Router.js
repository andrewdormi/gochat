const logger = require('../utils/logger');

class Router {
    constructor(controller) {
        this.controller = controller;
    }

    addSocket(socket) {
        if (!this.events) {
            return logger.info('Declare events in router!');
        }
        this.events.forEach((event) => {
            const method = event.split(':')[1] || event;
            if (!this.controller[method]) {
                return logger.info(`There no method for event: ${event}`);
            }
            socket.on(event, (data, cb)=> this.controller[method](socket, data, cb));
        });
    }
}

module.exports = Router;
const mongoose = require('mongoose');
const config = require('../config');
const logger = require('../utils/logger');

mongoose.connect(config.db);

mongoose.connection.on('connected', () => {
    logger.info(`Mongoose connection open on ${config.db}`);
});

mongoose.connection.on('error', err => {
    logger.error(`Mongoose connection error: ${err}`);
});

mongoose.connection.on('disconnected', () => {
    logger.error('Mongoose connection disconnected');
});

module.exports = mongoose.connection;

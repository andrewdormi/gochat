const User = require('../model/User');

class UserRepository {
    async findBySocialInfo(socialNetwork, socialId) {
        return await User.find({socialNetwork, socialId});
    }

    async create(firstName, lastName, socialNetwork, socialId, socialToken) {
        const newUser = await User.create({firstName, lastName, socialNetwork, socialId, socialToken});
        return await newUser.save();
    }

    async update(user, data) {
        await user.update(data);
        return await user.save();
    }
}

module.exports = new UserRepository();
const winston = require('winston');

const logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)()
    ],
    // exitOnError: false
});

logger.setLevels(winston.config.syslog.levels);
winston.addColors(winston.config.syslog.colors);

var loggingLevels = ['info', 'debug', 'warn', 'error'];
var exportedLogger = {};
loggingLevels.forEach((el)=> {
    exportedLogger[el] = function (message) {
        logger.log(el, message);
    }
});

module.exports = exportedLogger;
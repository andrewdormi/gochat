const fetch = require('node-fetch');
const UserRepository = require('../repository/User');

const VK_USER_GET = 'https://api.vk.com/method/users.get?access_token=';

class Authorization {
    async authWithVk(data, cb) {
        const {mid, sid} = data;
        if (!mid || !sid) {
            throw new Error({message: 'Missing authorisation info', code: 500});
        }

        const res = await fetch(`${VK_USER_GET}${sid}`);
        const resData = await res.json();
        if (resData.error) {
            throw new Error({message: 'Failed to get user info', code: 500});
        }
        const userInfo = resData.response[0];
        const {first_name, last_name, uid} = userInfo;

        const existedUser = await UserRepository.findBySocialInfo('vk', uid);
        if (existedUser.length) {
            await UserRepository.update(existedUser[0], {socialToken: sid});
            return existedUser[0].toObject();
        }
        const newUser = await UserRepository.create(first_name, last_name, 'vk', uid, sid);
        return newUser.toObject();
    }
}

module.exports = new Authorization();
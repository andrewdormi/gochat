module.exports = {
    db: 'mongodb://127.0.0.1:27017/gochat',
    port: 8080,
    redis: '127.0.0.1:6379'
};
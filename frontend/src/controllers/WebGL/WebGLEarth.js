import WebGL from './WebGL';
import {mat3, mat4, vec3} from 'gl-matrix';
import sphereFragmentShader from './shaders/fragment/sphere';
import sphereVertexShader from './shaders/vertex/sphere';

import EarthTextureImage from './textures/Earth/EarthColored.jpg';

export default class WebGLEarth extends WebGL {
    constructor(canvas) {
        super(canvas);
        this.initVars();
        this.initShaders();
        this.initAttributes();
        this.initBuffers();
        this.reset();

        this.earthTexture = this.gl.createTexture();
        this.loadTexture(this.earthTexture, EarthTextureImage).then((texture)=> {
            this.handleLoadedTexture(texture);
            this.tick();
        });
    }

    initVars() {
        this.pMatrix = mat4.create(); // perspective matrix
        this.mvMatrix = mat4.create(); // move matrix
        this.rotMatrix = mat4.create(); // rotation matrix
        mat4.identity(this.rotMatrix);
        mat4.rotate(this.rotMatrix, this.rotMatrix, Math.PI / 2, [0, 1, 0]);

        const lightingDirection = [1.0, -1.0, -0.5];
        this.adjustedLD = vec3.create();
        vec3.normalize(this.adjustedLD, lightingDirection);
        vec3.scale(this.adjustedLD, this.adjustedLD, -1);
    }

    drawScene() {
        const vw = this.gl.viewportWidth;
        const vh = this.gl.viewportHeight;

        this.clear();
        mat4.perspective(this.pMatrix, Math.PI / 4, vw / vh, 0.1, 100.0);
        this.gl.uniform1i(this.program.useLightingUniform, true);
        this.gl.uniform3f(this.program.ambientColorUniform, 0.0, 0.0, 0.0);

        this.gl.uniform3fv(this.program.lightingDirectionUniform, this.adjustedLD);
        this.gl.uniform3f(this.program.directionalColorUniform, 1.0, 1.0, 1.0);

        mat4.identity(this.mvMatrix);
        mat4.translate(this.mvMatrix, this.mvMatrix, [2.5, -1.7, -7]);
        mat4.multiply(this.mvMatrix, this.mvMatrix, this.rotMatrix);

        this.gl.activeTexture(this.gl.TEXTURE0);
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.earthTexture);
        this.gl.uniform1i(this.program.samplerUniform, 0);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.earthVertexPositionBuffer);
        this.gl.vertexAttribPointer(this.program.vertexPositionAttribute, this.earthVertexPositionBuffer.itemSize, this.gl.FLOAT, false, 0, 0);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.earthVertexTextureCoordBuffer);
        this.gl.vertexAttribPointer(this.program.textureCoordAttribute, this.earthVertexTextureCoordBuffer.itemSize, this.gl.FLOAT, false, 0, 0);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.earthVertexNormalBuffer);
        this.gl.vertexAttribPointer(this.program.vertexNormalAttribute, this.earthVertexNormalBuffer.itemSize, this.gl.FLOAT, false, 0, 0);

        this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.earthVertexIndexBuffer);
        this.setMatrixUniforms();
        this.gl.drawElements(this.gl.TRIANGLES, this.earthVertexIndexBuffer.numItems, this.gl.UNSIGNED_SHORT, 0);
    }

    setMatrixUniforms() {
        this.gl.uniformMatrix4fv(this.program.pMatrixUniform, false, this.pMatrix);
        this.gl.uniformMatrix4fv(this.program.mvMatrixUniform, false, this.mvMatrix);
        let normalMatrix = mat3.create();
        mat3.fromMat4(normalMatrix, this.mvMatrix);
        mat3.invert(normalMatrix, normalMatrix);
        mat3.transpose(normalMatrix, normalMatrix);
        this.gl.uniformMatrix3fv(this.program.nMatrixUniform, false, normalMatrix);
    }

    initShaders() {
        this.createProgram();
        this.attachShaders(this.gl.FRAGMENT_SHADER, sphereFragmentShader);
        this.attachShaders(this.gl.VERTEX_SHADER, sphereVertexShader);
        this.linkProgram();

        if (!this.gl.getProgramParameter(this.program, this.gl.LINK_STATUS)) {
            console.log('Could not initialise shaders');
            return;
        }
        this.useProgram();
    }

    initAttributes() {
        this.program.vertexPositionAttribute = this.gl.getAttribLocation(this.program, 'aVertexPosition');
        this.gl.enableVertexAttribArray(this.program.vertexPositionAttribute);
        this.program.textureCoordAttribute = this.gl.getAttribLocation(this.program, 'aTextureCoord');
        this.gl.enableVertexAttribArray(this.program.textureCoordAttribute);
        this.program.vertexNormalAttribute = this.gl.getAttribLocation(this.program, 'aVertexNormal');
        this.gl.enableVertexAttribArray(this.program.vertexNormalAttribute);

        this.program.pMatrixUniform = this.gl.getUniformLocation(this.program, 'uPMatrix');
        this.program.mvMatrixUniform = this.gl.getUniformLocation(this.program, 'uMVMatrix');
        this.program.nMatrixUniform = this.gl.getUniformLocation(this.program, 'uNMatrix');
        this.program.samplerUniform = this.gl.getUniformLocation(this.program, 'uSampler');
        this.program.useLightingUniform = this.gl.getUniformLocation(this.program, 'uUseLighting');
        this.program.ambientColorUniform = this.gl.getUniformLocation(this.program, 'uAmbientColor');
        this.program.lightingDirectionUniform = this.gl.getUniformLocation(this.program, 'uLightingDirection');
        this.program.directionalColorUniform = this.gl.getUniformLocation(this.program, 'uDirectionalColor');
    }

    handleLoadedTexture(texture) {
        this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, true);
        this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
        this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, texture.image);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR_MIPMAP_NEAREST);
        this.gl.generateMipmap(this.gl.TEXTURE_2D);
        this.gl.bindTexture(this.gl.TEXTURE_2D, null);
    }

    initBuffers() {
        const latitudeBands = 30;
        const longitudeBands = 30;
        const radius = 1;
        let vertexPositionData = [];
        let normalData = [];
        let textureCoordData = [];
        for (let latNumber = 0; latNumber <= latitudeBands; latNumber++) {
            let theta = latNumber * Math.PI / latitudeBands;
            let sinTheta = Math.sin(theta);
            let cosTheta = Math.cos(theta);
            for (let longNumber = 0; longNumber <= longitudeBands; longNumber++) {
                let phi = longNumber * 2 * Math.PI / longitudeBands;
                let sinPhi = Math.sin(phi);
                let cosPhi = Math.cos(phi);
                let x = cosPhi * sinTheta;
                let y = cosTheta;
                let z = sinPhi * sinTheta;
                let u = 1 - (longNumber / longitudeBands);
                let v = 1 - (latNumber / latitudeBands);
                normalData.push(x);
                normalData.push(y);
                normalData.push(z);
                textureCoordData.push(u);
                textureCoordData.push(v);
                vertexPositionData.push(radius * x);
                vertexPositionData.push(radius * y);
                vertexPositionData.push(radius * z);
            }
        }

        let indexData = [];
        for (let latNumber = 0; latNumber < latitudeBands; latNumber++) {
            for (let longNumber = 0; longNumber < longitudeBands; longNumber++) {
                let first = (latNumber * (longitudeBands + 1)) + longNumber;
                let second = first + longitudeBands + 1;
                indexData.push(first);
                indexData.push(second);
                indexData.push(first + 1);
                indexData.push(second);
                indexData.push(second + 1);
                indexData.push(first + 1);
            }
        }

        this.earthVertexNormalBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.earthVertexNormalBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(normalData), this.gl.STATIC_DRAW);
        this.earthVertexNormalBuffer.itemSize = 3;
        this.earthVertexNormalBuffer.numItems = normalData.length / 3;

        this.earthVertexTextureCoordBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.earthVertexTextureCoordBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(textureCoordData), this.gl.STATIC_DRAW);
        this.earthVertexTextureCoordBuffer.itemSize = 2;
        this.earthVertexTextureCoordBuffer.numItems = textureCoordData.length / 2;

        this.earthVertexPositionBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.earthVertexPositionBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(vertexPositionData), this.gl.STATIC_DRAW);
        this.earthVertexPositionBuffer.itemSize = 3;
        this.earthVertexPositionBuffer.numItems = vertexPositionData.length / 3;

        this.earthVertexIndexBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.earthVertexIndexBuffer);
        this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexData), this.gl.STATIC_DRAW);
        this.earthVertexIndexBuffer.itemSize = 1;
        this.earthVertexIndexBuffer.numItems = indexData.length;
    }

    nextTickTransform() {
        const time = new Date();
        const rotation = ((2 * Math.PI) / 60) * time.getSeconds() + ((2 * Math.PI) / 60000) * time.getMilliseconds();
        mat4.identity(this.rotMatrix);

        mat4.rotate(this.rotMatrix, this.rotMatrix, rotation, [0, 1, 0]);
    }
}
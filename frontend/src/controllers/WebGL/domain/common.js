export function getWebGLContext(canvas) {
    let context;

    try {
        context = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');
    }
    catch (e) {
    }

    return context;
}
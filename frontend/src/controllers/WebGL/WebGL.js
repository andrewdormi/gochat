import {getWebGLContext} from './domain/common';

import requestAnimationFrame from '../../utils/requestAnimationFrame';

const DEFAULT_FPS = 60;

export default class WebGL {
    constructor(canvas) {
        this.canvas = canvas;
        this.fps = DEFAULT_FPS;
        this.gl = getWebGLContext(canvas);
        if (!this.gl) {
            console.warn('doesnt support webgl');
        }
        this.initialSetup();
    }

    reset() {
        this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
        this.gl.enable(this.gl.DEPTH_TEST);
    }

    clear() {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    }

    tick() {
        requestAnimationFrame(this.tick.bind(this));
        this.drawScene();
        this.nextTickTransform();
    }

    drawScene() {
        console.warn('method drawScene need to be overriden');
    }

    nextTickTransform() {

    }

    initialSetup() {
        this.updateViewPort();
    }

    updateViewPort() {
        this.gl.viewportWidth = this.canvas.width;
        this.gl.viewportHeight = this.canvas.height;
        this.gl.viewport(0, 0, this.canvas.width, this.canvas.height);
    }

    createProgram() {
        this.program = this.gl.createProgram();
    }

    linkProgram() {
        this.gl.linkProgram(this.program);
    }

    useProgram() {
        this.gl.useProgram(this.program);
    }

    attachShaders(type, src) {
        const shader = this.gl.createShader(type);
        this.gl.shaderSource(shader, src);
        this.gl.compileShader(shader);

        if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)) {
            console.log(this.gl.getShaderInfoLog(shader));
            return;
        }

        this.gl.attachShader(this.program, shader);
    }

    loadTexture(texture, src) {
        return new Promise((resolve, reject)=> {
            texture.image = new Image();
            texture.image.onload = function () {
                resolve(texture);
            };
            texture.image.src = src;
        });
    }
}
import WebGL from '../WebGL';

export const DEFAULT_POSITION = [
    -1, -1,
    -1, 1,
    1, -1,
    1, -1,
    -1, 1,
    1, 1
];

export const DEFAULT_TEXTURE_COORDS = [
    0, 0,
    0, 1,
    1, 0,
    1, 0,
    0, 1,
    1, 1
];

export default class Filter extends WebGL {
    constructor(source, canvas) {
        super(canvas);
        this.source = source;

        this.initVars();
        this.initTexture();
        this.bindDefaultBuffers();
    }

    initVars() {

    }

    updateSizes() {
        this.canvas.width = this.source.clientWidth;
        this.canvas.height = this.source.clientHeight;
        this.updateViewPort();
    }

    initTexture() {
        this.texture = this.gl.createTexture();

        this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
    }

    updateTexture() {
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
        this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, true);
        this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, false);
        this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, this.source);
    }

    initShaders() {
        this.createProgram();
        this.attachShaders(this.gl.FRAGMENT_SHADER, this.fragmentShader);
        this.attachShaders(this.gl.VERTEX_SHADER, this.verdexShader);
        this.linkProgram();

        if (!this.gl.getProgramParameter(this.program, this.gl.LINK_STATUS)) {
            console.log('Could not initialise shaders');
            return;
        }
        this.useProgram();
    }

    bindDefaultBuffers() {
        this.positionBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.positionBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(DEFAULT_POSITION), this.gl.STATIC_DRAW);

        this.texcoordBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.texcoordBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(DEFAULT_TEXTURE_COORDS), this.gl.STATIC_DRAW);
    }

    applyDefaultBuffers() {
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.positionBuffer);
        this.gl.enableVertexAttribArray(this.positionLocation);
        this.gl.vertexAttribPointer(this.positionLocation, 2, this.gl.FLOAT, false, 0, 0);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.texcoordBuffer);
        this.gl.enableVertexAttribArray(this.texcoordLocation);
        this.gl.vertexAttribPointer(this.texcoordLocation, 2, this.gl.FLOAT, false, 0, 0);

        this.gl.uniform1i(this.textureLocation, 0);
    }

    drawShape() {
        this.gl.drawArrays(this.gl.TRIANGLES, 0, 6);
    }
}
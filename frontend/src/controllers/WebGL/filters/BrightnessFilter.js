import Filter from './Filter';
import BrightnessFilterFragment from '../shaders/fragment/BrightnessFilterFragment';
import BrightnessFilterVertex from '../shaders/vertex/BrightnessFilterVertex';

export default class BrighnessFilter extends Filter {
    constructor(source, canvas, data) {
        super(source, canvas);
        this.filterData = data;
        this.loadShaders();
        this.loadVars();
        this.tick();
    }

    loadVars() {
        this.positionLocation = this.gl.getAttribLocation(this.program, 'aVertexPosition');
        this.texcoordLocation = this.gl.getAttribLocation(this.program, 'aTextureCoord');
        this.textureLocation = this.gl.getUniformLocation(this.program, 'source');

        this.brightness = this.gl.getUniformLocation(this.program, 'brightness');
        this.contrast = this.gl.getUniformLocation(this.program, 'contrast');
    }

    loadShaders() {
        this.fragmentShader = BrightnessFilterFragment;
        this.verdexShader = BrightnessFilterVertex;
        this.initShaders();
    }

    drawScene() {
        this.updateSizes();
        this.updateTexture();
        this.applyDefaultBuffers();

        this.gl.uniform1f(this.brightness, this.filterData.brightness);
        this.gl.uniform1f(this.contrast, this.filterData.contrast);

        this.drawShape();
    }
}
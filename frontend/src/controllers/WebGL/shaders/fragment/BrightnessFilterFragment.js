const BrightnessFilterFragment = `
    precision mediump float;

	varying vec2 vTextureCoord;
	uniform sampler2D source;
	uniform float brightness;
	uniform float contrast;
	const vec3 half3 = vec3(0.5);
	void main(void) {
		vec4 pixel = texture2D(source, vTextureCoord);
		vec3 color = pixel.rgb * brightness;
		color = (color - half3) * contrast + half3;
		gl_FragColor = vec4(color, pixel.a);
	}
`;

export default BrightnessFilterFragment;

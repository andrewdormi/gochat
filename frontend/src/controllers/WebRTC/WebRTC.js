import getUserMedia from './domain/getUserMedia';

class WebRTC {
    constructor() {
        this.localStream = null;
    }

    getLocalStream() {
        return new Promise((resolve, reject)=> {
            if (this.localStream) {
                return resolve(this.localStream);
            }
            getUserMedia().then((stream)=> {
                this.localStream = stream;
                resolve(stream);
            }).catch(reject);
        });
    }
}

export default new WebRTC();
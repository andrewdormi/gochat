export default class Haar {
    constructor() {
    }

    groupElement(r1, r2) {
        var distance = (r1.width * 0.25 + 0.5) | 0;

        return r2.x <= r1.x + distance &&
            r2.x >= r1.x - distance &&
            r2.y <= r1.y + distance &&
            r2.y >= r1.y - distance &&
            r2.width <= (r1.width * 1.5 + 0.5) | 0 &&
            (r2.width * 1.5 + 0.5) | 0 >= r1.width;
    }

    detect_single_scale(int_sum, int_sqsum, int_tilted, width, height, scale, classifier) {
        const win_w = (classifier.size[0] * scale) | 0;
        const win_h = (classifier.size[1] * scale) | 0;
        const step_x = (0.5 * scale + 1.5) | 0;
        const step_y = step_x;

        let ex = (width - win_w) | 0, ey = (height - win_h) | 0;
        let w1 = (width + 1) | 0;
        const inv_area = 1.0 / (win_w * win_h);
        let fi_a, fi_b, fi_c, fi_d, fw, fh;

        var ii_a = 0, ii_b = win_w, ii_c = win_h * w1, ii_d = ii_c + win_w;

        let rects = [];
        for (let y = 0; y < ey; y += step_y) {
            ii_a = y * w1;
            for (let x = 0; x < ex; x += step_x, ii_a += step_x) {
                const mean = (int_sum[ii_a] - int_sum[ii_a + ii_b] - int_sum[ii_a + ii_c] + int_sum[ii_a + ii_d]) * inv_area;

                const variance = (int_sqsum[ii_a] - int_sqsum[ii_a + ii_b] - int_sqsum[ii_a + ii_c] + int_sqsum[ii_a + ii_d]) * inv_area - mean * mean;

                const std = variance > 0 ? Math.sqrt(variance) : 1;

                const stages = classifier.complexClassifiers;
                let found = true;
                let stage_sum = 0;
                for (const stage in stages) {
                    const trees = stage.simpleClassifiers;

                    for (const tree in trees) {
                        let tree_sum = 0;
                        const features = tree.features;

                        if (tree.tilted === 1) {
                            for (const feature in features) {
                                fi_a = ~~(x + feature[0] * scale) + ~~(y + feature[1] * scale) * w1;
                                fw = ~~(feature[2] * scale);
                                fh = ~~(feature[3] * scale);
                                fi_b = fw * w1;
                                fi_c = fh * w1;

                                tree_sum += (int_tilted[fi_a]
                                    - int_tilted[fi_a + fw + fi_b]
                                    - int_tilted[fi_a - fh + fi_c]
                                    + int_tilted[fi_a + fw - fh + fi_b + fi_c]) * feature[4];
                            }
                        } else {
                            for (const feature in features) {
                                fi_a = ~~(x + feature[0] * scale) + ~~(y + feature[1] * scale) * w1;
                                fw = ~~(feature[2] * scale);
                                fh = ~~(feature[3] * scale);
                                fi_c = fh * w1;

                                tree_sum += (int_sum[fi_a]
                                    - int_sum[fi_a + fw]
                                    - int_sum[fi_a + fi_c]
                                    + int_sum[fi_a + fi_c + fw]) * feature[4];
                            }
                        }
                        stage_sum += (tree_sum * inv_area < tree.threshold * std) ? tree.left_val : tree.right_val;
                    }
                    if (stage_sum < stage.threshold) {
                        found = false;
                        break;
                    }
                }

                if (found) {
                    rects.push({
                        x,
                        y,
                        width: win_w,
                        height: win_h,
                        neighbor: 1,
                        confidence: stage_sum
                    });
                    x += step_x;
                    ii_a += step_x;
                }
            }
        }
        return rects;
    }

    detect_multi_scale(int_sum, int_sqsum, int_tilted, width, height, classifier, scale_factor, scale_min) {
        scale_factor = scale_factor || 1.2;
        scale_min = scale_min || 1.0;
        const win_w = classifier.size[0];
        const win_h = classifier.size[1];
        let rects = [];
        while (scale_min * win_w < width && scale_min * win_h < height) {
            const rect = this.detect_single_scale(int_sum, int_sqsum, int_tilted, width, height, scale_min, classifier);
            rects = rects.concat(rect);
            scale_min *= scale_factor;
        }
        return rects;
    }

    group_rectangles(rects, min_neighbors) {
        min_neighbors = min_neighbors || 1;
        var i, j, n = rects.length;
        var node = [];
        for (const rect in rects) {
            node.push({parent: -1, element: rect, rank: 0});
        }

        for (i = 0; i < n; ++i) {
            if (!node[i].element)
                continue;
            var root = i;
            while (node[root].parent != -1)
                root = node[root].parent;
            for (j = 0; j < n; ++j) {
                if (i != j && node[j].element && this.groupElement(node[i].element, node[j].element)) {
                    var root2 = j;

                    while (node[root2].parent != -1)
                        root2 = node[root2].parent;

                    if (root2 != root) {
                        if (node[root].rank > node[root2].rank)
                            node[root2].parent = root;
                        else {
                            node[root].parent = root2;
                            if (node[root].rank == node[root2].rank)
                                node[root2].rank++;
                            root = root2;
                        }

                        /* compress path from node2 to the root: */
                        var temp, node2 = j;
                        while (node[node2].parent != -1) {
                            temp = node2;
                            node2 = node[node2].parent;
                            node[temp].parent = root;
                        }

                        /* compress path from node to the root: */
                        node2 = i;
                        while (node[node2].parent != -1) {
                            temp = node2;
                            node2 = node[node2].parent;
                            node[temp].parent = root;
                        }
                    }
                }
            }
        }
        var idx_seq = [];
        var class_idx = 0;
        for (i = 0; i < n; i++) {
            j = -1;
            var node1 = i;
            if (node[node1].element) {
                while (node[node1].parent != -1)
                    node1 = node[node1].parent;
                if (node[node1].rank >= 0)
                    node[node1].rank = ~class_idx++;
                j = ~node[node1].rank;
            }
            idx_seq[i] = j;
        }

        var comps = [];
        for (i = 0; i < class_idx + 1; ++i) {
            comps[i] = {
                'neighbors': 0,
                'x': 0,
                'y': 0,
                'width': 0,
                'height': 0,
                'confidence': 0
            };
        }

        // count number of neighbors
        for (i = 0; i < n; ++i) {
            var r1 = rects[i];
            var idx = idx_seq[i];

            if (comps[idx].neighbors == 0)
                comps[idx].confidence = r1.confidence;

            ++comps[idx].neighbors;

            comps[idx].x += r1.x;
            comps[idx].y += r1.y;
            comps[idx].width += r1.width;
            comps[idx].height += r1.height;
            comps[idx].confidence = Math.max(comps[idx].confidence, r1.confidence);
        }

        var seq2 = [];
        // calculate average bounding box
        for (i = 0; i < class_idx; ++i) {
            n = comps[i].neighbors;
            if (n >= min_neighbors)
                seq2.push({
                    'x': (comps[i].x * 2 + n) / (2 * n),
                    'y': (comps[i].y * 2 + n) / (2 * n),
                    'width': (comps[i].width * 2 + n) / (2 * n),
                    'height': (comps[i].height * 2 + n) / (2 * n),
                    'neighbors': comps[i].neighbors,
                    'confidence': comps[i].confidence
                });
        }

        var result_seq = [];
        n = seq2.length;
        // filter out small face rectangles inside large face rectangles
        for (i = 0; i < n; ++i) {
            var r1 = seq2[i];
            var flag = true;
            for (j = 0; j < n; ++j) {
                var r2 = seq2[j];
                var distance = (r2.width * 0.25 + 0.5) | 0;

                if (i != j &&
                    r1.x >= r2.x - distance &&
                    r1.y >= r2.y - distance &&
                    r1.x + r1.width <= r2.x + r2.width + distance &&
                    r1.y + r1.height <= r2.y + r2.height + distance &&
                    (r2.neighbors > Math.max(3, r1.neighbors) || r1.neighbors < 3)) {
                    flag = false;
                    break;
                }
            }

            if (flag)
                result_seq.push(r1);
        }
        return result_seq;
    }
}
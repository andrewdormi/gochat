import config from '../../config';
import io from 'socket.io-client';
import {EventEmitter} from 'events';

class Socket extends EventEmitter {
    connect() {
        this.socket = io(config.serverUrl);
        this.socket.on('connect', this.subscribe);
    }

    subscribe = ()=> {

    };

    emitToServer(message, data, cb) {
        this.socket.emit(message, data, cb);
    }
}

export default new Socket();

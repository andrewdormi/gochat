import {EventEmitter} from 'events';
import cookie from 'js-cookie';
import Socket from '../Socket/Socket';
import socketEvents from '../../const/socketEvents';
const VK = window.VK;

class Auth extends EventEmitter {
    loginWithVk() {
        VK.Auth.login((response)=> {
            if (response.session) {
                this.emit('authSuccess', {type: 'vk', ...response.session});
            }
        });
    }

    setUser(data) {
        this.user = data;
        cookie.set('user', this.user);
        this.emit('userInfo', this.user);
    }

    checkAuth() {
        return new Promise((resolve, reject)=> {
            const user = cookie.get('user');
            if (!user) {
                return reject();
            }
            VK.Auth.getLoginStatus((response)=> {
                if (response.session) {
                    const userData = {type: 'vk', ...response.session};
                    Socket.emitToServer(socketEvents.auth, userData, (err, data)=> {
                        if (err) {
                            return reject();
                        }
                        this.setUser(data);
                        resolve();
                    });
                } else {
                    reject();
                }
            });
        });
    }
}

export default new Auth();

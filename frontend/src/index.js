import React from 'react';
import ReactDOM from 'react-dom';
import Router from './Router/Router';
import Socket from './controllers/Socket/Socket';

import initSocialApps from './helpers/social/initSocialApps';

import './index.css';

Socket.connect();
initSocialApps();

ReactDOM.render(
    <Router />,
    document.getElementById('root')
);

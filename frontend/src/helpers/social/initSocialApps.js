import config from '../../config';
const VK = window.VK;

function initSocialApps() {
    VK.init({apiId: config.vkApp.id});
}

export default initSocialApps;
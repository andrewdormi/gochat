import BrighnessFilter from '../controllers/WebGL/filters/BrightnessFilter';

export default {
    first: {
        filter: BrighnessFilter,
        data: {
            brightness: 1.2,
            contrast: 1.0
        }
    }
}
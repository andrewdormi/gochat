import MainChatStateDefault from '../components/MainChat/MainChatStateDefault/MainChatStateDefault';
import MainChatStateWaiting from '../components/MainChat/MainChatStateWaiting/MainChatStateWaiting';

export default {
    default: {
        name: 'default',
        component: MainChatStateDefault
    },
    waiting: {
        name: 'waiting',
        component: MainChatStateWaiting
    }
};
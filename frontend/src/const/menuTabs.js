import React from 'react';
import ChatIcon from '../components/common/icons/header/chat';
import SettingsIcon from '../components/common/icons/header/settings';

import MainChatContainer from '../containers/MainChatContainer/MainChatContainer';
import SettingsContainer from '../containers/SettingsContainer/SettingsContainer';

export default{
    chat: {
        name: 'chat',
        label: 'Chat',
        icon: <ChatIcon className="headerIcon"/>,
        contentContainer: MainChatContainer
    },
    settings: {
        name: 'settings',
        label: 'Settings',
        icon: <SettingsIcon className="headerIcon"/>,
        contentContainer: SettingsContainer
    }
};
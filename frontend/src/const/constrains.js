export default {
    video: {
        width: {ideal: 640},
        height: {ideal: 480}
    },
    audio: true
};

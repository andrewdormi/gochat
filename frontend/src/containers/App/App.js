import React, {Component} from 'react';

import './App.css';

export default class App extends Component {
    componentDidMount() {
    }

    render() {
        const {children} = this.props;

        return (
            <div className="App">
                {children}
            </div>
        );
    }
}

import React, {Component} from 'react';
import EarthBackground from '../../components/EarthBackground/EarthBackground';
import LoginComponent from '../../components/Login/Login';

import './Login.css';

export default class Login extends Component {
    render() {
        return (
            <div className="Login">
                <EarthBackground />
                <LoginComponent />
            </div>
        );
    }
}

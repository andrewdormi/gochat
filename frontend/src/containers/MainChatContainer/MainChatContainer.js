import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';

import CHAT_STATES from '../../const/mainChatStates';

import './MainChatContainer.css';

export default class MainChatContainer extends Component {
    static propTypes = {
        className: PropTypes.string,
        shown: PropTypes.bool
    };

    state = {
        state: CHAT_STATES.default,
    };

    resolveContent() {
        const {state} = this.state;
        return React.createElement(state.component, {});
    }

    render() {
        const {shown, className} = this.props;
        const containerClass = classnames('MainChatContainer', className, !shown ? 'hidden' : '');
        const content = this.resolveContent();
        return (
            <div className={containerClass}>
                {content}
            </div>
        );
    }
}

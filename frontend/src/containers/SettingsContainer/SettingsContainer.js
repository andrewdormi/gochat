import React, {Component, PropTypes} from 'react';
import CameraSettings from '../../components/CameraSettings/CameraSettings';
import classnames from 'classnames';

import './SettingsContainer.css';

export default class SettingsContainer extends Component {
    static propTypes = {
        className: PropTypes.string,
        shown: PropTypes.bool
    };

    render() {
        const {shown, className} = this.props;
        const containerClass = classnames('SettingsContainer', className, !shown ? 'hidden' : '');
        return (
            <div className={containerClass}>
                {shown && <CameraSettings />}
            </div>
        );
    }
}

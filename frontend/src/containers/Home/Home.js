import React, {Component} from 'react';
import Header from '../../components/Header/Header';

import TABS from '../../const/menuTabs';
import './Home.css';

export default class Home extends Component {
    state = {
        selectedTab: TABS.chat.name
    };

    handleSelectTab = (selectedTab)=> {
        this.setState({selectedTab});
    };

    renderContent(tab, key) {
        const {selectedTab} = this.state;
        const shown = selectedTab === tab.name;
        return React.createElement(tab.contentContainer, {shown, key, className: 'contentContainerComponent'});
    }

    render() {
        const {selectedTab} = this.state;
        const content = Object.keys(TABS).map((key, i)=>this.renderContent(TABS[key], i));

        return (
            <div className="Home">
                <Header selectedTab={selectedTab} handleSelectTab={this.handleSelectTab}/>
                <div className="ContentContainer">
                    {content}
                </div>
            </div>
        );
    }
}

import React, {Component} from 'react';
import WebGLEarth from '../../controllers/WebGL/WebGLEarth';

import './EarthBackground.css';

export default class EarthBackground extends Component {
    componentDidMount() {
        this.WebGLEarth = new WebGLEarth(this.refs.canvas);
        window.addEventListener('resize', this.onResize.bind(this));
    }

    componentDidUpdate() {
        this.WebGLEarth.updateViewPort();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize.bind(this));
    }

    onResize() {
        this.forceUpdate();
    }

    render() {
        const w = window.innerWidth;
        const h = window.innerHeight;
        return (
            <div className="EarthBackground" ref="container">
                <canvas ref="canvas"
                        className="EarthCanvas"
                        width={w}
                        height={h}
                ></canvas>
            </div>
        );
    }
}

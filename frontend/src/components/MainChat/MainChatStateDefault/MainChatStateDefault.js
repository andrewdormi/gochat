import React, {Component} from 'react';
import Search from '../../common/icons/search';
import ActionButton from '../../common/ActionButton/ActionButton';

import './MainChatStateDefault.css';

export default class MainChatStateDefault extends Component {
    render() {
        return (
            <div className="MainChatStateDefault">
                <ActionButton label="Search"
                              className="centerActionButton"
                              icon={<Search className="buttonIcon"/>}
                />
            </div>
        );
    }
}

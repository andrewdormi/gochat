import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';

import './ActionButton.css';

export default class ActionButton extends Component {
    static propsTypes = {
        className: PropTypes.string,
        icon: PropTypes.any,
        label: PropTypes.string,
        onClick: PropTypes.func
    };

    render() {
        const {icon, label, onClick, className} = this.props;
        const containerClass = classnames('ActionButton', className);

        return (
            <div className={containerClass} onClick={onClick}>
                {icon}
                <span>{label}</span>
            </div>
        );
    }
}

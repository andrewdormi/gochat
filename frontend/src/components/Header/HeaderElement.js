import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';

import './Header.css';

export default class HeaderElement extends Component {
    static propsTypes = {
        label: PropTypes.string,
        icon: PropTypes.any,
        isSelected: PropTypes.bool,
        handleSelectTab: PropTypes.func
    };

    render() {
        const {label, icon, isSelected, handleSelectTab}= this.props;
        const className = classnames('HeaderElement', isSelected ? 'SelectedElement' : '');
        return (
            <div className={className} onClick={handleSelectTab}>
                {icon}
                <span>{label}</span>
            </div>
        );
    }
}

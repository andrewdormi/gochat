import React, {Component, PropTypes} from 'react';
import HeaderElement from './HeaderElement';

import TABS from '../../const/menuTabs';

import './Header.css';

export default class Header extends Component {
    static propsTypes = {
        selectedTab: PropTypes.string,
        handleSelectTab: PropTypes.func
    };

    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    renderTabs(tab, i) {
        const {handleSelectTab} = this.props;
        const isSelected = this.props.selectedTab === tab.name;
        return (
            <HeaderElement label={tab.label}
                           icon={tab.icon}
                           isSelected={isSelected}
                           key={i}
                           handleSelectTab={handleSelectTab.bind(this, tab.name)}
            />
        );
    }

    render() {
        const tabs = Object.keys(TABS).map((key, i)=>this.renderTabs(TABS[key], i));
        return (
            <div className="Header">
                {tabs}
            </div>
        );
    }
}

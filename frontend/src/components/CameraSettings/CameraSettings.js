import React, {Component} from 'react';
import WerRTC from '../../controllers/WebRTC/WebRTC';
import UserVideo from './UserVideo/UserVideo';
import filters from '../../const/filters';
import './CameraSettings.css';

export default class CameraSettings extends Component {
    state = {
        localStream: null
    };

    componentDidMount() {
        WerRTC.getLocalStream().then(this.handleLocalStream);
    }

    handleLocalStream = (localStream)=> {
        this.setState({localStream});
    };

    render() {
        const {localStream} = this.state;

        return (
            <div className="CameraSettings">
                <UserVideo className="SettingsUserVideo"
                           stream={localStream}
                           muted={true}
                           filter={filters.first}/>
            </div>
        );
    }
}

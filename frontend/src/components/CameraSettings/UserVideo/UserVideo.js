import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import './UserVideo.css';

export default class UserVideo extends Component {
    static propsTypes = {
        className: PropTypes.string,
        filter: PropTypes.object,
        stream: PropTypes.object,
        muted: PropTypes.bool
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.stream) {
            const {video} = this.refs;
            video.srcObject = nextProps.stream;
            video.addEventListener('canplay', this.applyFilter);
            video.play();
        }
    }

    componentWillUnmount() {
        this.refs.video.removeEventListener('canplay', this.applyFilter);
    }

    applyFilter = ()=> {
        const {filter} = this.props;
        const {video, canvas} = this.refs;
        new filter.filter(video, canvas, filter.data);
    };

    render() {
        const {muted, className} = this.props;
        const containerClass = classnames('UserVideo', className);
        return (
            <div className={containerClass}>
                <canvas ref="canvas"/>
                <video muted={muted} ref="video"/>
            </div>
        );
    }
}

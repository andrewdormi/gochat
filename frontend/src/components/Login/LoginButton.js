import React, {Component, PropTypes} from 'react';

import './Login.css';

export default class LoginButton extends Component {
    static propTypes = {
        icon: PropTypes.any,
        onClick: PropTypes.func
    };

    render() {
        const {icon, onClick} = this.props;
        return (
            <div className="socialLoginButton" onClick={onClick}>
                {icon}
            </div>
        );
    }
}

import React, {Component, PropTypes} from 'react';
import Auth from '../../controllers/Auth/Auth';
import Socket from '../../controllers/Socket/Socket';
import socketEvents from '../../const/socketEvents';

import LoginButton from './LoginButton';
import VkIcon from '../common/icons/social/vk';

import './Login.css';

export default class Login extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    componentDidMount() {
        this.events();
    }

    componentWillUnmount() {
        this.events(true);
    }

    events(unSubsctibe) {
        const method = unSubsctibe ? 'removeListener' : 'on';
        Auth[method]('authSuccess', this.onAuthSuccess);
    }

    onAuthSuccess = (data)=> {
        Socket.emitToServer(socketEvents.auth, data, this.onAuthorizationDone);
    };

    onAuthorizationDone = (err, data)=> {
        console.log('user data', err, data);
        Auth.setUser(data);
        this.context.router.push('/');
    };

    loginWithVk = ()=> {
        Auth.loginWithVk();
    };

    render() {
        return (
            <div className="LoginWithSocialContainer">
                <div className="LoginHeader">Login with some social network</div>
                <div>
                    <LoginButton
                        onClick={this.loginWithVk}
                        icon={<VkIcon className="socialIcon" fill="white"/>}
                    />
                </div>
            </div>
        );
    }
}

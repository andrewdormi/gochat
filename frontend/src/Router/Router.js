import React, {Component} from 'react';
import {Router, IndexRoute, Route, browserHistory} from 'react-router';

import Auth from '../controllers/Auth/Auth';

import App from '../containers/App/App';
import Home from '../containers/Home/Home';
import Login from '../containers/Login/Login';

export default class AppRouter extends Component {
    requireLogin = (nextState, replace, cb) => {
        Auth.checkAuth().then(cb).catch(()=> {
            replace('/login');
            cb();
        });
    };

    render() {
        return (
            <Router history={browserHistory}>
                <Route onEnter={this.requireLogin} path="/" component={App}>
                    <IndexRoute components={Home}/>
                </Route>
                <Route path="login" component={Login}/>
            </Router>
        );
    }
}
